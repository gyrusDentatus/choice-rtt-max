#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v2023.2.3),
    on Tue Feb 13 15:58:09 2024
If you publish work using this script the most relevant publication is:

    Peirce J, Gray JR, Simpson S, MacAskill M, Höchenberger R, Sogo H, Kastman E, Lindeløv JK. (2019) 
        PsychoPy2: Experiments in behavior made easy Behav Res 51: 195. 
        https://doi.org/10.3758/s13428-018-01193-y

"""
""" Ilja """
""" Ilja 2"""

""" Ilja 4"""

""" Ilja 5""


# --- Import packages ---
from psychopy import locale_setup
from psychopy import prefs
from psychopy import plugins
plugins.activatePlugins()
prefs.hardware['audioLib'] = 'ptb'
prefs.hardware['audioLatencyMode'] = '3'
from psychopy import sound, gui, visual, core, data, event, logging, clock, colors, layout, iohub, hardware
from psychopy.tools import environmenttools
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER, priority)

import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle, choice as randchoice
import os  # handy system and path functions
import sys  # to get file system encoding

import psychopy.iohub as io
from psychopy.hardware import keyboard

# --- Setup global variables (available in all functions) ---
# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
# Store info about the experiment session
psychopyVersion = '2023.2.3'
expName = 'Crtt_exp'  # from the Builder filename that created this script
expInfo = {
    'participant': '',
    'session': '',
    'left-handed': True,
    'age': '',
    'Do you like this session?': ['Yes', 'No','Hell No'],
    'output-path': '',
    'date': data.getDateStr(),  # add a simple timestamp
    'expName': expName,
    'psychopyVersion': psychopyVersion,
}


def showExpInfoDlg(expInfo):
    """
    Show participant info dialog.
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    
    Returns
    ==========
    dict
        Information about this experiment.
    """
    # temporarily remove keys which the dialog doesn't need to show
    poppedKeys = {
        'date': expInfo.pop('date', data.getDateStr()),
        'expName': expInfo.pop('expName', expName),
        'psychopyVersion': expInfo.pop('psychopyVersion', psychopyVersion),
    }
    # show participant info dialog
    dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
    if dlg.OK == False:
        core.quit()  # user pressed cancel
    # restore hidden keys
    expInfo.update(poppedKeys)
    # return expInfo
    return expInfo


def setupData(expInfo, dataDir=None):
    """
    Make an ExperimentHandler to handle trials and saving.
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    dataDir : Path, str or None
        Folder to save the data to, leave as None to create a folder in the current directory.    
    Returns
    ==========
    psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    """
    
    # data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
    if dataDir is None:
        dataDir = _thisDir
    filename = u'%s/sub-%s/ses-%s/%s_%s_%s_%s' % (expInfo['output-path'], expInfo['participant'], expInfo['session'], expInfo['participant'], expInfo['session'], expName, expInfo['date'])  
    # make sure filename is relative to dataDir
    if os.path.isabs(filename):
        dataDir = os.path.commonprefix([dataDir, filename])
        filename = os.path.relpath(filename, dataDir)
    
    # an ExperimentHandler isn't essential but helps with data saving
    thisExp = data.ExperimentHandler(
        name=expName, version='',
        extraInfo=expInfo, runtimeInfo=None,
        originPath='/Users/alexandergottker/Desktop/NOWA_school/choice_rtt/code/experiment/Crtt_exp.py',
        savePickle=True, saveWideText=True,
        dataFileName=dataDir + os.sep + filename, sortColumns='time'
    )
    thisExp.setPriority('thisRow.t', priority.CRITICAL)
    thisExp.setPriority('expName', priority.LOW)
    # return experiment handler
    return thisExp


def setupLogging(filename):
    """
    Setup a log file and tell it what level to log at.
    
    Parameters
    ==========
    filename : str or pathlib.Path
        Filename to save log file and data files as, doesn't need an extension.
    
    Returns
    ==========
    psychopy.logging.LogFile
        Text stream to receive inputs from the logging system.
    """
    # this outputs to the screen, not a file
    logging.console.setLevel(logging.EXP)
    # save a log file for detail verbose info
    logFile = logging.LogFile(filename+'.log', level=logging.EXP)
    
    return logFile


def setupWindow(expInfo=None, win=None):
    """
    Setup the Window
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    win : psychopy.visual.Window
        Window to setup - leave as None to create a new window.
    
    Returns
    ==========
    psychopy.visual.Window
        Window in which to run this experiment.
    """
    if win is None:
        # if not given a window to setup, make one
        win = visual.Window(
            size=[1440, 900], fullscr=True, screen=0,
            winType='pyglet', allowStencil=False,
            monitor='testMonitor', color=[0,0,0], colorSpace='rgb',
            backgroundImage='', backgroundFit='none',
            blendMode='avg', useFBO=True,
            units='height'
        )
        if expInfo is not None:
            # store frame rate of monitor if we can measure it
            expInfo['frameRate'] = win.getActualFrameRate()
    else:
        # if we have a window, just set the attributes which are safe to set
        win.color = [0,0,0]
        win.colorSpace = 'rgb'
        win.backgroundImage = ''
        win.backgroundFit = 'none'
        win.units = 'height'
    win.mouseVisible = False
    win.hideMessage()
    return win


def setupInputs(expInfo, thisExp, win):
    """
    Setup whatever inputs are available (mouse, keyboard, eyetracker, etc.)
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    win : psychopy.visual.Window
        Window in which to run this experiment.
    Returns
    ==========
    dict
        Dictionary of input devices by name.
    """
    # --- Setup input devices ---
    inputs = {}
    ioConfig = {}
    
    # Setup iohub keyboard
    ioConfig['Keyboard'] = dict(use_keymap='psychopy')
    
    ioSession = '1'
    if 'session' in expInfo:
        ioSession = str(expInfo['session'])
    ioServer = io.launchHubServer(window=win, experiment_code='Crtt_exp', session_code=ioSession, datastore_name=thisExp.dataFileName, **ioConfig)
    eyetracker = None
    
    # create a default keyboard (e.g. to check for escape)
    defaultKeyboard = keyboard.Keyboard(backend='iohub')
    # return inputs dict
    return {
        'ioServer': ioServer,
        'defaultKeyboard': defaultKeyboard,
        'eyetracker': eyetracker,
    }

def pauseExperiment(thisExp, inputs=None, win=None, timers=[], playbackComponents=[]):
    """
    Pause this experiment, preventing the flow from advancing to the next routine until resumed.
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    inputs : dict
        Dictionary of input devices by name.
    win : psychopy.visual.Window
        Window for this experiment.
    timers : list, tuple
        List of timers to reset once pausing is finished.
    playbackComponents : list, tuple
        List of any components with a `pause` method which need to be paused.
    """
    # if we are not paused, do nothing
    if thisExp.status != PAUSED:
        return
    
    # pause any playback components
    for comp in playbackComponents:
        comp.pause()
    # prevent components from auto-drawing
    win.stashAutoDraw()
    # run a while loop while we wait to unpause
    while thisExp.status == PAUSED:
        # make sure we have a keyboard
        if inputs is None:
            inputs = {
                'defaultKeyboard': keyboard.Keyboard(backend='ioHub')
            }
        # check for quit (typically the Esc key)
        if inputs['defaultKeyboard'].getKeys(keyList=['escape']):
            endExperiment(thisExp, win=win, inputs=inputs)
        # flip the screen
        win.flip()
    # if stop was requested while paused, quit
    if thisExp.status == FINISHED:
        endExperiment(thisExp, inputs=inputs, win=win)
    # resume any playback components
    for comp in playbackComponents:
        comp.play()
    # restore auto-drawn components
    win.retrieveAutoDraw()
    # reset any timers
    for timer in timers:
        timer.reset()


def run(expInfo, thisExp, win, inputs, globalClock=None, thisSession=None):
    """
    Run the experiment flow.
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    psychopy.visual.Window
        Window in which to run this experiment.
    inputs : dict
        Dictionary of input devices by name.
    globalClock : psychopy.core.clock.Clock or None
        Clock to get global time from - supply None to make a new one.
    thisSession : psychopy.session.Session or None
        Handle of the Session object this experiment is being run from, if any.
    """
    # mark experiment as started
    thisExp.status = STARTED
    # make sure variables created by exec are available globally
    exec = environmenttools.setExecEnvironment(globals())
    # get device handles from dict of input devices
    ioServer = inputs['ioServer']
    defaultKeyboard = inputs['defaultKeyboard']
    eyetracker = inputs['eyetracker']
    # make sure we're running in the directory for this experiment
    os.chdir(_thisDir)
    # get filename from ExperimentHandler for convenience
    filename = thisExp.dataFileName
    frameTolerance = 0.001  # how close to onset before 'same' frame
    endExpNow = False  # flag for 'escape' or other condition => quit the exp
    # get frame duration from frame rate in expInfo
    if 'frameRate' in expInfo and expInfo['frameRate'] is not None:
        frameDur = 1.0 / round(expInfo['frameRate'])
    else:
        frameDur = 1.0 / 60.0  # could not measure, so guess
    
    # Start Code - component code to be run after the window creation
    
    # --- Initialize components for Routine "WelcomeIlja" ---
    welcome_message = visual.TextStim(win=win, name='welcome_message',
        text='Welcome to the awesome Experiment! \n\nPlease press the space bar to continue',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color=[-1.0000, -1.0000, -1.0000], colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    polygon = visual.Rect(
        win=win, name='polygon',
        width=(2, 0.2)[0], height=(2, 0.2)[1],
        ori=0.0, pos=(0, -0.4), anchor='center',
        lineWidth=1.0,     colorSpace='rgb',  lineColor=[1.0000, 1.0000, 1.0000], fillColor=[0.6000, 1.0000, 1.0000],
        opacity=None, depth=-1.0, interpolate=True)
    dots = visual.DotStim(
        win=win, name='dots',
        nDots=1000, dotSize=10.0,
        speed=1.0, dir=45.0, coherence=0.8,
        fieldPos=(0,0), fieldSize=1.0, fieldAnchor='center', fieldShape='circle',
        signalDots='same', noiseDots='direction',dotLife=5.0,
        color=[-1.0000, -1.0000, 1.0000], colorSpace='rgb', opacity=None,
        depth=-2.0)
    spacebar_welcome = keyboard.Keyboard()
    sound_1 = sound.Sound('4800', secs=-1, stereo=True, hamming=True,
        name='sound_1')
    sound_1.setVolume(1.0)
    
    # --- Initialize components for Routine "InstructionsforIlja" ---
    text = visual.TextStim(win=win, name='text',
        text='In this task, you will make decisions as to which stimulus you have seen. There are three versions of the task. In the first, you have to decide which shape you have seen, in the second which image and the third which sound you’ve heard. Before each task, you will get set of specific instructions and short practice period.\n\nPlease press the space bar to continue',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color=[-0.2000, 0.8000, -0.8000], colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    key_resp = keyboard.Keyboard()
    polygon_2 = visual.ShapeStim(
        win=win, name='polygon_2',
        size=(0.2, 0.2), vertices='circle',
        ori=0.0, pos=[0,0], anchor='center',
        lineWidth=1.0,     colorSpace='rgb',  lineColor='white', fillColor='white',
        opacity=None, depth=-2.0, interpolate=True)
    image = visual.ImageStim(
        win=win,
        name='image', 
        image='/Users/alexandergottker/Desktop/NOWA_school/choice_rtt/stimuli/Ilja.jpeg', mask=None, anchor='center',
        ori=0.0, pos=(0, -0.3), size=(0.5, 0.5),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-3.0)
    
    # --- Initialize components for Routine "Instructions_shape" ---
    text_3 = visual.TextStim(win=win, name='text_3',
        text='In this task you will make a decision as to which shape you have seen.\n\nPress C or click cross for a cross Press V or click square for a square Press B or click plus for a plus\n\nFirst, we will have a quick practice.\n\nPush space bar or click / touch one of the buttons to begin.',
        font='Open Sans',
        pos=(0, 0.1), height=0.035, wrapWidth=None, ori=0.0, 
        color=[-0.2000, 0.8000, 1.0000], colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    key_resp_3 = keyboard.Keyboard()
    Response_Cross = visual.ImageStim(
        win=win,
        name='Response_Cross', 
        image='/Users/alexandergottker/Desktop/NOWA_school/choice_rtt/stimuli/response_cross.jpg', mask=None, anchor='center',
        ori=0.0, pos=(-0.25, -0.25), size=(0.2,0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-2.0)
    Response_Plus = visual.ImageStim(
        win=win,
        name='Response_Plus', 
        image='/Users/alexandergottker/Desktop/NOWA_school/choice_rtt/stimuli/response_plus.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-3.0)
    Response_Square = visual.ImageStim(
        win=win,
        name='Response_Square', 
        image='/Users/alexandergottker/Desktop/NOWA_school/choice_rtt/stimuli/response_square.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-4.0)
    
    # --- Initialize components for Routine "trial" ---
    VisualReminder_Cross = visual.ImageStim(
        win=win,
        name='VisualReminder_Cross', 
        image='/Users/alexandergottker/Desktop/NOWA_school/choice_rtt/stimuli/response_cross.jpg', mask=None, anchor='center',
        ori=0.0, pos=(-0.25, -0.25), size=(0.2,0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-1.0)
    VisualReminder_Plus = visual.ImageStim(
        win=win,
        name='VisualReminder_Plus', 
        image='/Users/alexandergottker/Desktop/NOWA_school/choice_rtt/stimuli/response_plus.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-2.0)
    VisualReminder_Square = visual.ImageStim(
        win=win,
        name='VisualReminder_Square', 
        image='/Users/alexandergottker/Desktop/NOWA_school/choice_rtt/stimuli/response_square.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-3.0)
    leftfar_Frame = visual.ImageStim(
        win=win,
        name='leftfar_Frame', 
        image='/Users/alexandergottker/Desktop/NOWA_school/choice_rtt/stimuli/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-4.0)
    leftFrame = visual.ImageStim(
        win=win,
        name='leftFrame', 
        image='/Users/alexandergottker/Desktop/NOWA_school/choice_rtt/stimuli/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-5.0)
    rightfar_Frame = visual.ImageStim(
        win=win,
        name='rightfar_Frame', 
        image='/Users/alexandergottker/Desktop/NOWA_school/choice_rtt/stimuli/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(0.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-6.0)
    rightFrame = visual.ImageStim(
        win=win,
        name='rightFrame', 
        image='/Users/alexandergottker/Desktop/NOWA_school/choice_rtt/stimuli/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-7.0)
    target_image = visual.ImageStim(
        win=win,
        name='target_image', 
        image='default.png', mask=None, anchor='center',
        ori=0.0, pos=[0,0], size=(0.2, 0.2),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-8.0)
    keyboard_response = keyboard.Keyboard()
    
    # --- Initialize components for Routine "FeedBack" ---
    Feedback = visual.TextStim(win=win, name='Feedback',
        text='',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    FeedBackResp = keyboard.Keyboard()
    
    # --- Initialize components for Routine "EndScreen" ---
    text_2 = visual.TextStim(win=win, name='text_2',
        text='You have reached the end of the experiment, thank you very much for participating. \n\nPlease press the space bar to finish.\n',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color=[-0.2000, 0.6000, -0.6000], colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    key_resp_2 = keyboard.Keyboard()
    
    # create some handy timers
    if globalClock is None:
        globalClock = core.Clock()  # to track the time since experiment started
    if ioServer is not None:
        ioServer.syncClock(globalClock)
    logging.setDefaultClock(globalClock)
    routineTimer = core.Clock()  # to track time remaining of each (possibly non-slip) routine
    win.flip()  # flip window to reset last flip timer
    # store the exact time the global clock started
    expInfo['expStart'] = data.getDateStr(format='%Y-%m-%d %Hh%M.%S.%f %z', fractionalSecondDigits=6)
    
    # --- Prepare to start Routine "WelcomeIlja" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('WelcomeIlja.started', globalClock.getTime())
    dots.refreshDots()
    spacebar_welcome.keys = []
    spacebar_welcome.rt = []
    _spacebar_welcome_allKeys = []
    sound_1.setSound('4800', secs=2.5, hamming=True)
    sound_1.setVolume(1.0, log=False)
    sound_1.seek(0)
    # Run 'Begin Routine' code from CreateDirectories
    # Construct the path with placeholders replaced by actual values
    dir_path = expInfo['output-path'] + "/sub-" + expInfo['participant'] + "/ses-" + expInfo['session']
    
    # Check if the path exists
    if not os.path.exists(dir_path):
        # Create the directory, including all intermediate directories
        os.makedirs(path)
        print(path)
        print(dir_path)
        print(f"Directory '{dir_path}' was created.")
    else:
        print(f"Directory '{dir_path}' already exists.")
    # keep track of which components have finished
    WelcomeIljaComponents = [welcome_message, polygon, dots, spacebar_welcome, sound_1]
    for thisComponent in WelcomeIljaComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "WelcomeIlja" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *welcome_message* updates
        
        # if welcome_message is starting this frame...
        if welcome_message.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            welcome_message.frameNStart = frameN  # exact frame index
            welcome_message.tStart = t  # local t and not account for scr refresh
            welcome_message.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(welcome_message, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'welcome_message.started')
            # update status
            welcome_message.status = STARTED
            welcome_message.setAutoDraw(True)
        
        # if welcome_message is active this frame...
        if welcome_message.status == STARTED:
            # update params
            pass
        
        # *polygon* updates
        
        # if polygon is starting this frame...
        if polygon.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            polygon.frameNStart = frameN  # exact frame index
            polygon.tStart = t  # local t and not account for scr refresh
            polygon.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(polygon, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'polygon.started')
            # update status
            polygon.status = STARTED
            polygon.setAutoDraw(True)
        
        # if polygon is active this frame...
        if polygon.status == STARTED:
            # update params
            pass
        
        # *dots* updates
        
        # if dots is starting this frame...
        if dots.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
            # keep track of start time/frame for later
            dots.frameNStart = frameN  # exact frame index
            dots.tStart = t  # local t and not account for scr refresh
            dots.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(dots, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'dots.started')
            # update status
            dots.status = STARTED
            dots.setAutoDraw(True)
        
        # if dots is active this frame...
        if dots.status == STARTED:
            # update params
            pass
        
        # if dots is stopping this frame...
        if dots.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > dots.tStartRefresh + 2.5-frameTolerance:
                # keep track of stop time/frame for later
                dots.tStop = t  # not accounting for scr refresh
                dots.frameNStop = frameN  # exact frame index
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'dots.stopped')
                # update status
                dots.status = FINISHED
                dots.setAutoDraw(False)
        
        # *spacebar_welcome* updates
        waitOnFlip = False
        
        # if spacebar_welcome is starting this frame...
        if spacebar_welcome.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            spacebar_welcome.frameNStart = frameN  # exact frame index
            spacebar_welcome.tStart = t  # local t and not account for scr refresh
            spacebar_welcome.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(spacebar_welcome, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'spacebar_welcome.started')
            # update status
            spacebar_welcome.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(spacebar_welcome.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(spacebar_welcome.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if spacebar_welcome.status == STARTED and not waitOnFlip:
            theseKeys = spacebar_welcome.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _spacebar_welcome_allKeys.extend(theseKeys)
            if len(_spacebar_welcome_allKeys):
                spacebar_welcome.keys = _spacebar_welcome_allKeys[-1].name  # just the last key pressed
                spacebar_welcome.rt = _spacebar_welcome_allKeys[-1].rt
                spacebar_welcome.duration = _spacebar_welcome_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # if sound_1 is starting this frame...
        if sound_1.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
            # keep track of start time/frame for later
            sound_1.frameNStart = frameN  # exact frame index
            sound_1.tStart = t  # local t and not account for scr refresh
            sound_1.tStartRefresh = tThisFlipGlobal  # on global time
            # add timestamp to datafile
            thisExp.addData('sound_1.started', tThisFlipGlobal)
            # update status
            sound_1.status = STARTED
            sound_1.play(when=win)  # sync with win flip
        
        # if sound_1 is stopping this frame...
        if sound_1.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > sound_1.tStartRefresh + 2.5-frameTolerance:
                # keep track of stop time/frame for later
                sound_1.tStop = t  # not accounting for scr refresh
                sound_1.frameNStop = frameN  # exact frame index
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'sound_1.stopped')
                # update status
                sound_1.status = FINISHED
                sound_1.stop()
        # update sound_1 status according to whether it's playing
        if sound_1.isPlaying:
            sound_1.status = STARTED
        elif sound_1.isFinished:
            sound_1.status = FINISHED
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in WelcomeIljaComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "WelcomeIlja" ---
    for thisComponent in WelcomeIljaComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('WelcomeIlja.stopped', globalClock.getTime())
    # check responses
    if spacebar_welcome.keys in ['', [], None]:  # No response was made
        spacebar_welcome.keys = None
    thisExp.addData('spacebar_welcome.keys',spacebar_welcome.keys)
    if spacebar_welcome.keys != None:  # we had a response
        thisExp.addData('spacebar_welcome.rt', spacebar_welcome.rt)
        thisExp.addData('spacebar_welcome.duration', spacebar_welcome.duration)
    thisExp.nextEntry()
    sound_1.pause()  # ensure sound has stopped at end of Routine
    # the Routine "WelcomeIlja" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # --- Prepare to start Routine "InstructionsforIlja" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('InstructionsforIlja.started', globalClock.getTime())
    key_resp.keys = []
    key_resp.rt = []
    _key_resp_allKeys = []
    # keep track of which components have finished
    InstructionsforIljaComponents = [text, key_resp, polygon_2, image]
    for thisComponent in InstructionsforIljaComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "InstructionsforIlja" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *text* updates
        
        # if text is starting this frame...
        if text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            text.frameNStart = frameN  # exact frame index
            text.tStart = t  # local t and not account for scr refresh
            text.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'text.started')
            # update status
            text.status = STARTED
            text.setAutoDraw(True)
        
        # if text is active this frame...
        if text.status == STARTED:
            # update params
            pass
        
        # *key_resp* updates
        waitOnFlip = False
        
        # if key_resp is starting this frame...
        if key_resp.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            key_resp.frameNStart = frameN  # exact frame index
            key_resp.tStart = t  # local t and not account for scr refresh
            key_resp.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(key_resp, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'key_resp.started')
            # update status
            key_resp.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(key_resp.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(key_resp.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if key_resp.status == STARTED and not waitOnFlip:
            theseKeys = key_resp.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _key_resp_allKeys.extend(theseKeys)
            if len(_key_resp_allKeys):
                key_resp.keys = _key_resp_allKeys[-1].name  # just the last key pressed
                key_resp.rt = _key_resp_allKeys[-1].rt
                key_resp.duration = _key_resp_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # *polygon_2* updates
        
        # if polygon_2 is starting this frame...
        if polygon_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            polygon_2.frameNStart = frameN  # exact frame index
            polygon_2.tStart = t  # local t and not account for scr refresh
            polygon_2.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(polygon_2, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'polygon_2.started')
            # update status
            polygon_2.status = STARTED
            polygon_2.setAutoDraw(True)
        
        # if polygon_2 is active this frame...
        if polygon_2.status == STARTED:
            # update params
            polygon_2.setPos((0+1*t,0), log=False)
        
        # if polygon_2 is stopping this frame...
        if polygon_2.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > polygon_2.tStartRefresh + 2.0-frameTolerance:
                # keep track of stop time/frame for later
                polygon_2.tStop = t  # not accounting for scr refresh
                polygon_2.frameNStop = frameN  # exact frame index
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'polygon_2.stopped')
                # update status
                polygon_2.status = FINISHED
                polygon_2.setAutoDraw(False)
        
        # *image* updates
        
        # if image is starting this frame...
        if image.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            image.frameNStart = frameN  # exact frame index
            image.tStart = t  # local t and not account for scr refresh
            image.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(image, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'image.started')
            # update status
            image.status = STARTED
            image.setAutoDraw(True)
        
        # if image is active this frame...
        if image.status == STARTED:
            # update params
            pass
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in InstructionsforIljaComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "InstructionsforIlja" ---
    for thisComponent in InstructionsforIljaComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('InstructionsforIlja.stopped', globalClock.getTime())
    # check responses
    if key_resp.keys in ['', [], None]:  # No response was made
        key_resp.keys = None
    thisExp.addData('key_resp.keys',key_resp.keys)
    if key_resp.keys != None:  # we had a response
        thisExp.addData('key_resp.rt', key_resp.rt)
        thisExp.addData('key_resp.duration', key_resp.duration)
    thisExp.nextEntry()
    # the Routine "InstructionsforIlja" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # --- Prepare to start Routine "Instructions_shape" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('Instructions_shape.started', globalClock.getTime())
    key_resp_3.keys = []
    key_resp_3.rt = []
    _key_resp_3_allKeys = []
    # keep track of which components have finished
    Instructions_shapeComponents = [text_3, key_resp_3, Response_Cross, Response_Plus, Response_Square]
    for thisComponent in Instructions_shapeComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "Instructions_shape" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *text_3* updates
        
        # if text_3 is starting this frame...
        if text_3.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            text_3.frameNStart = frameN  # exact frame index
            text_3.tStart = t  # local t and not account for scr refresh
            text_3.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text_3, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'text_3.started')
            # update status
            text_3.status = STARTED
            text_3.setAutoDraw(True)
        
        # if text_3 is active this frame...
        if text_3.status == STARTED:
            # update params
            pass
        
        # *key_resp_3* updates
        waitOnFlip = False
        
        # if key_resp_3 is starting this frame...
        if key_resp_3.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            key_resp_3.frameNStart = frameN  # exact frame index
            key_resp_3.tStart = t  # local t and not account for scr refresh
            key_resp_3.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(key_resp_3, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'key_resp_3.started')
            # update status
            key_resp_3.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(key_resp_3.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(key_resp_3.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if key_resp_3.status == STARTED and not waitOnFlip:
            theseKeys = key_resp_3.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _key_resp_3_allKeys.extend(theseKeys)
            if len(_key_resp_3_allKeys):
                key_resp_3.keys = _key_resp_3_allKeys[-1].name  # just the last key pressed
                key_resp_3.rt = _key_resp_3_allKeys[-1].rt
                key_resp_3.duration = _key_resp_3_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # *Response_Cross* updates
        
        # if Response_Cross is starting this frame...
        if Response_Cross.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            Response_Cross.frameNStart = frameN  # exact frame index
            Response_Cross.tStart = t  # local t and not account for scr refresh
            Response_Cross.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(Response_Cross, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'Response_Cross.started')
            # update status
            Response_Cross.status = STARTED
            Response_Cross.setAutoDraw(True)
        
        # if Response_Cross is active this frame...
        if Response_Cross.status == STARTED:
            # update params
            pass
        
        # *Response_Plus* updates
        
        # if Response_Plus is starting this frame...
        if Response_Plus.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            Response_Plus.frameNStart = frameN  # exact frame index
            Response_Plus.tStart = t  # local t and not account for scr refresh
            Response_Plus.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(Response_Plus, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'Response_Plus.started')
            # update status
            Response_Plus.status = STARTED
            Response_Plus.setAutoDraw(True)
        
        # if Response_Plus is active this frame...
        if Response_Plus.status == STARTED:
            # update params
            pass
        
        # *Response_Square* updates
        
        # if Response_Square is starting this frame...
        if Response_Square.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            Response_Square.frameNStart = frameN  # exact frame index
            Response_Square.tStart = t  # local t and not account for scr refresh
            Response_Square.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(Response_Square, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'Response_Square.started')
            # update status
            Response_Square.status = STARTED
            Response_Square.setAutoDraw(True)
        
        # if Response_Square is active this frame...
        if Response_Square.status == STARTED:
            # update params
            pass
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in Instructions_shapeComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "Instructions_shape" ---
    for thisComponent in Instructions_shapeComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('Instructions_shape.stopped', globalClock.getTime())
    # check responses
    if key_resp_3.keys in ['', [], None]:  # No response was made
        key_resp_3.keys = None
    thisExp.addData('key_resp_3.keys',key_resp_3.keys)
    if key_resp_3.keys != None:  # we had a response
        thisExp.addData('key_resp_3.rt', key_resp_3.rt)
        thisExp.addData('key_resp_3.duration', key_resp_3.duration)
    thisExp.nextEntry()
    # the Routine "Instructions_shape" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # set up handler to look after randomisation of conditions etc
    PracticeLoop = data.TrialHandler(nReps=1.0, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=data.importConditions('shapes_targets.csv'),
        seed=None, name='PracticeLoop')
    thisExp.addLoop(PracticeLoop)  # add the loop to the experiment
    thisPracticeLoop = PracticeLoop.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisPracticeLoop.rgb)
    if thisPracticeLoop != None:
        for paramName in thisPracticeLoop:
            globals()[paramName] = thisPracticeLoop[paramName]
    
    for thisPracticeLoop in PracticeLoop:
        currentLoop = PracticeLoop
        thisExp.timestampOnFlip(win, 'thisRow.t')
        # pause experiment here if requested
        if thisExp.status == PAUSED:
            pauseExperiment(
                thisExp=thisExp, 
                inputs=inputs, 
                win=win, 
                timers=[routineTimer], 
                playbackComponents=[]
        )
        # abbreviate parameter names if possible (e.g. rgb = thisPracticeLoop.rgb)
        if thisPracticeLoop != None:
            for paramName in thisPracticeLoop:
                globals()[paramName] = thisPracticeLoop[paramName]
        
        # --- Prepare to start Routine "trial" ---
        continueRoutine = True
        # update component parameters for each repeat
        # Run 'Begin Routine' code from JitterThings
        # list of possible onsets for target 
        list_onset = [1, 1.2, 1.4, 1.6, 1.8]
        
        # Randoize these
        shuffle(list_onset)
        onset_trial = list_onset[0] # pick the first one
        
        # list of possible positions 
        list_positions = [-0.375, -0.125, 0.125, 0.375]
        shuffle(list_positions)
        position_trial = list_positions[0] # pick the first one
        
        #path of where to find the target image
        if TargetImage == '../../stimuli/target_square.jpg': #path of where to find the target image
            #setting the key press that will be the correct answer
            corrAns = 'v' 
        #path of where to find the target image
        elif TargetImage == '../../stimuli/target_cross.jpg':
            #setting the key press that will be the correct answer
            corrAns = 'c'
        #path of where to find the target image
        elif TargetImage == '../../stimuli/target_plus.jpg':
            #setting the key press that will be the correct answer
            corrAns = 'b'
        
        
        thisExp.addData('trial.started', globalClock.getTime())
        target_image.setPos((position_trial, 0))
        target_image.setImage(TargetImage)
        keyboard_response.keys = []
        keyboard_response.rt = []
        _keyboard_response_allKeys = []
        # keep track of which components have finished
        trialComponents = [VisualReminder_Cross, VisualReminder_Plus, VisualReminder_Square, leftfar_Frame, leftFrame, rightfar_Frame, rightFrame, target_image, keyboard_response]
        for thisComponent in trialComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "trial" ---
        routineForceEnded = not continueRoutine
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *VisualReminder_Cross* updates
            
            # if VisualReminder_Cross is starting this frame...
            if VisualReminder_Cross.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                VisualReminder_Cross.frameNStart = frameN  # exact frame index
                VisualReminder_Cross.tStart = t  # local t and not account for scr refresh
                VisualReminder_Cross.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(VisualReminder_Cross, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'VisualReminder_Cross.started')
                # update status
                VisualReminder_Cross.status = STARTED
                VisualReminder_Cross.setAutoDraw(True)
            
            # if VisualReminder_Cross is active this frame...
            if VisualReminder_Cross.status == STARTED:
                # update params
                pass
            
            # *VisualReminder_Plus* updates
            
            # if VisualReminder_Plus is starting this frame...
            if VisualReminder_Plus.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                VisualReminder_Plus.frameNStart = frameN  # exact frame index
                VisualReminder_Plus.tStart = t  # local t and not account for scr refresh
                VisualReminder_Plus.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(VisualReminder_Plus, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'VisualReminder_Plus.started')
                # update status
                VisualReminder_Plus.status = STARTED
                VisualReminder_Plus.setAutoDraw(True)
            
            # if VisualReminder_Plus is active this frame...
            if VisualReminder_Plus.status == STARTED:
                # update params
                pass
            
            # *VisualReminder_Square* updates
            
            # if VisualReminder_Square is starting this frame...
            if VisualReminder_Square.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                VisualReminder_Square.frameNStart = frameN  # exact frame index
                VisualReminder_Square.tStart = t  # local t and not account for scr refresh
                VisualReminder_Square.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(VisualReminder_Square, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'VisualReminder_Square.started')
                # update status
                VisualReminder_Square.status = STARTED
                VisualReminder_Square.setAutoDraw(True)
            
            # if VisualReminder_Square is active this frame...
            if VisualReminder_Square.status == STARTED:
                # update params
                pass
            
            # *leftfar_Frame* updates
            
            # if leftfar_Frame is starting this frame...
            if leftfar_Frame.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                leftfar_Frame.frameNStart = frameN  # exact frame index
                leftfar_Frame.tStart = t  # local t and not account for scr refresh
                leftfar_Frame.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(leftfar_Frame, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'leftfar_Frame.started')
                # update status
                leftfar_Frame.status = STARTED
                leftfar_Frame.setAutoDraw(True)
            
            # if leftfar_Frame is active this frame...
            if leftfar_Frame.status == STARTED:
                # update params
                pass
            
            # *leftFrame* updates
            
            # if leftFrame is starting this frame...
            if leftFrame.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                leftFrame.frameNStart = frameN  # exact frame index
                leftFrame.tStart = t  # local t and not account for scr refresh
                leftFrame.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(leftFrame, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'leftFrame.started')
                # update status
                leftFrame.status = STARTED
                leftFrame.setAutoDraw(True)
            
            # if leftFrame is active this frame...
            if leftFrame.status == STARTED:
                # update params
                pass
            
            # *rightfar_Frame* updates
            
            # if rightfar_Frame is starting this frame...
            if rightfar_Frame.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                rightfar_Frame.frameNStart = frameN  # exact frame index
                rightfar_Frame.tStart = t  # local t and not account for scr refresh
                rightfar_Frame.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(rightfar_Frame, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'rightfar_Frame.started')
                # update status
                rightfar_Frame.status = STARTED
                rightfar_Frame.setAutoDraw(True)
            
            # if rightfar_Frame is active this frame...
            if rightfar_Frame.status == STARTED:
                # update params
                pass
            
            # *rightFrame* updates
            
            # if rightFrame is starting this frame...
            if rightFrame.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                rightFrame.frameNStart = frameN  # exact frame index
                rightFrame.tStart = t  # local t and not account for scr refresh
                rightFrame.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(rightFrame, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'rightFrame.started')
                # update status
                rightFrame.status = STARTED
                rightFrame.setAutoDraw(True)
            
            # if rightFrame is active this frame...
            if rightFrame.status == STARTED:
                # update params
                pass
            
            # *target_image* updates
            
            # if target_image is starting this frame...
            if target_image.status == NOT_STARTED and tThisFlip >= onset_trial-frameTolerance:
                # keep track of start time/frame for later
                target_image.frameNStart = frameN  # exact frame index
                target_image.tStart = t  # local t and not account for scr refresh
                target_image.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(target_image, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'target_image.started')
                # update status
                target_image.status = STARTED
                target_image.setAutoDraw(True)
            
            # if target_image is active this frame...
            if target_image.status == STARTED:
                # update params
                pass
            
            # if target_image is stopping this frame...
            if target_image.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > target_image.tStartRefresh + 0.2-frameTolerance:
                    # keep track of stop time/frame for later
                    target_image.tStop = t  # not accounting for scr refresh
                    target_image.frameNStop = frameN  # exact frame index
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_image.stopped')
                    # update status
                    target_image.status = FINISHED
                    target_image.setAutoDraw(False)
            
            # *keyboard_response* updates
            
            # if keyboard_response is starting this frame...
            if keyboard_response.status == NOT_STARTED and t >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                keyboard_response.frameNStart = frameN  # exact frame index
                keyboard_response.tStart = t  # local t and not account for scr refresh
                keyboard_response.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(keyboard_response, 'tStartRefresh')  # time at next scr refresh
                # update status
                keyboard_response.status = STARTED
                # keyboard checking is just starting
                keyboard_response.clock.reset()  # now t=0
                keyboard_response.clearEvents(eventType='keyboard')
            if keyboard_response.status == STARTED:
                theseKeys = keyboard_response.getKeys(keyList=['c','v','b'], ignoreKeys=["escape"], waitRelease=False)
                _keyboard_response_allKeys.extend(theseKeys)
                if len(_keyboard_response_allKeys):
                    keyboard_response.keys = _keyboard_response_allKeys[-1].name  # just the last key pressed
                    keyboard_response.rt = _keyboard_response_allKeys[-1].rt
                    keyboard_response.duration = _keyboard_response_allKeys[-1].duration
                    # was this correct?
                    if (keyboard_response.keys == str(corrAns)) or (keyboard_response.keys == corrAns):
                        keyboard_response.corr = 1
                    else:
                        keyboard_response.corr = 0
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if defaultKeyboard.getKeys(keyList=["escape"]):
                thisExp.status = FINISHED
            if thisExp.status == FINISHED or endExpNow:
                endExperiment(thisExp, inputs=inputs, win=win)
                return
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in trialComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "trial" ---
        for thisComponent in trialComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        thisExp.addData('trial.stopped', globalClock.getTime())
        # check responses
        if keyboard_response.keys in ['', [], None]:  # No response was made
            keyboard_response.keys = None
            # was no response the correct answer?!
            if str(corrAns).lower() == 'none':
               keyboard_response.corr = 1;  # correct non-response
            else:
               keyboard_response.corr = 0;  # failed to respond (incorrectly)
        # store data for PracticeLoop (TrialHandler)
        PracticeLoop.addData('keyboard_response.keys',keyboard_response.keys)
        PracticeLoop.addData('keyboard_response.corr', keyboard_response.corr)
        if keyboard_response.keys != None:  # we had a response
            PracticeLoop.addData('keyboard_response.rt', keyboard_response.rt)
            PracticeLoop.addData('keyboard_response.duration', keyboard_response.duration)
        # Run 'End Routine' code from computeRT_Acc
        #this code is to record the reaction times and accuracy of the trial
        
        thisRoutineDuration = t # how long did this trial last
        
        # keyboard_response.rt is the time with which a key was pressed
        # thisRecRT - how long it took for participants to respond after onset of target_image
        # thisAcc - whether or not the response was correct
        # compute RT based on onset of target
        thisRecRT = keyboard_response.rt - onset_trial 
        
        # check of the response was correct
        if keyboard_response.corr == 1: 
        
            # if it was correct, assign 'correct' value
            thisAcc = 'correct' 
        
        # if not, assign 'incorrect' value
        else:
            thisAcc = 'incorrect'
        
        # record the actual response times of each trial 
        thisExp.addData('trialRespTimes', thisRecRT) 
        # the Routine "trial" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        
        # --- Prepare to start Routine "FeedBack" ---
        continueRoutine = True
        # update component parameters for each repeat
        thisExp.addData('FeedBack.started', globalClock.getTime())
        Feedback.setText("The last recorded RT was: "  + str(round(thisRecRT, 3)) + " \nThe response was: "  + thisAcc + " \n\nPress the space bar to continue."  
        )
        FeedBackResp.keys = []
        FeedBackResp.rt = []
        _FeedBackResp_allKeys = []
        # keep track of which components have finished
        FeedBackComponents = [Feedback, FeedBackResp]
        for thisComponent in FeedBackComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "FeedBack" ---
        routineForceEnded = not continueRoutine
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *Feedback* updates
            
            # if Feedback is starting this frame...
            if Feedback.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                Feedback.frameNStart = frameN  # exact frame index
                Feedback.tStart = t  # local t and not account for scr refresh
                Feedback.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(Feedback, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'Feedback.started')
                # update status
                Feedback.status = STARTED
                Feedback.setAutoDraw(True)
            
            # if Feedback is active this frame...
            if Feedback.status == STARTED:
                # update params
                pass
            
            # *FeedBackResp* updates
            waitOnFlip = False
            
            # if FeedBackResp is starting this frame...
            if FeedBackResp.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                FeedBackResp.frameNStart = frameN  # exact frame index
                FeedBackResp.tStart = t  # local t and not account for scr refresh
                FeedBackResp.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(FeedBackResp, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'FeedBackResp.started')
                # update status
                FeedBackResp.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(FeedBackResp.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(FeedBackResp.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if FeedBackResp.status == STARTED and not waitOnFlip:
                theseKeys = FeedBackResp.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
                _FeedBackResp_allKeys.extend(theseKeys)
                if len(_FeedBackResp_allKeys):
                    FeedBackResp.keys = _FeedBackResp_allKeys[-1].name  # just the last key pressed
                    FeedBackResp.rt = _FeedBackResp_allKeys[-1].rt
                    FeedBackResp.duration = _FeedBackResp_allKeys[-1].duration
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if defaultKeyboard.getKeys(keyList=["escape"]):
                thisExp.status = FINISHED
            if thisExp.status == FINISHED or endExpNow:
                endExperiment(thisExp, inputs=inputs, win=win)
                return
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in FeedBackComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "FeedBack" ---
        for thisComponent in FeedBackComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        thisExp.addData('FeedBack.stopped', globalClock.getTime())
        # check responses
        if FeedBackResp.keys in ['', [], None]:  # No response was made
            FeedBackResp.keys = None
        PracticeLoop.addData('FeedBackResp.keys',FeedBackResp.keys)
        if FeedBackResp.keys != None:  # we had a response
            PracticeLoop.addData('FeedBackResp.rt', FeedBackResp.rt)
            PracticeLoop.addData('FeedBackResp.duration', FeedBackResp.duration)
        # the Routine "FeedBack" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        thisExp.nextEntry()
        
        if thisSession is not None:
            # if running in a Session with a Liaison client, send data up to now
            thisSession.sendExperimentData()
    # completed 1.0 repeats of 'PracticeLoop'
    
    
    # --- Prepare to start Routine "EndScreen" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('EndScreen.started', globalClock.getTime())
    key_resp_2.keys = []
    key_resp_2.rt = []
    _key_resp_2_allKeys = []
    # keep track of which components have finished
    EndScreenComponents = [text_2, key_resp_2]
    for thisComponent in EndScreenComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "EndScreen" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *text_2* updates
        
        # if text_2 is starting this frame...
        if text_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            text_2.frameNStart = frameN  # exact frame index
            text_2.tStart = t  # local t and not account for scr refresh
            text_2.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text_2, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'text_2.started')
            # update status
            text_2.status = STARTED
            text_2.setAutoDraw(True)
        
        # if text_2 is active this frame...
        if text_2.status == STARTED:
            # update params
            pass
        
        # *key_resp_2* updates
        waitOnFlip = False
        
        # if key_resp_2 is starting this frame...
        if key_resp_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            key_resp_2.frameNStart = frameN  # exact frame index
            key_resp_2.tStart = t  # local t and not account for scr refresh
            key_resp_2.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(key_resp_2, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'key_resp_2.started')
            # update status
            key_resp_2.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(key_resp_2.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(key_resp_2.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if key_resp_2.status == STARTED and not waitOnFlip:
            theseKeys = key_resp_2.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _key_resp_2_allKeys.extend(theseKeys)
            if len(_key_resp_2_allKeys):
                key_resp_2.keys = _key_resp_2_allKeys[-1].name  # just the last key pressed
                key_resp_2.rt = _key_resp_2_allKeys[-1].rt
                key_resp_2.duration = _key_resp_2_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in EndScreenComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "EndScreen" ---
    for thisComponent in EndScreenComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('EndScreen.stopped', globalClock.getTime())
    # check responses
    if key_resp_2.keys in ['', [], None]:  # No response was made
        key_resp_2.keys = None
    thisExp.addData('key_resp_2.keys',key_resp_2.keys)
    if key_resp_2.keys != None:  # we had a response
        thisExp.addData('key_resp_2.rt', key_resp_2.rt)
        thisExp.addData('key_resp_2.duration', key_resp_2.duration)
    thisExp.nextEntry()
    # the Routine "EndScreen" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # mark experiment as finished
    endExperiment(thisExp, win=win, inputs=inputs)


def saveData(thisExp):
    """
    Save data from this experiment
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    """
    filename = thisExp.dataFileName
    # these shouldn't be strictly necessary (should auto-save)
    thisExp.saveAsWideText(filename + '.csv', delim='auto')
    thisExp.saveAsPickle(filename)


def endExperiment(thisExp, inputs=None, win=None):
    """
    End this experiment, performing final shut down operations.
    
    This function does NOT close the window or end the Python process - use `quit` for this.
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    inputs : dict
        Dictionary of input devices by name.
    win : psychopy.visual.Window
        Window for this experiment.
    """
    if win is not None:
        # remove autodraw from all current components
        win.clearAutoDraw()
        # Flip one final time so any remaining win.callOnFlip() 
        # and win.timeOnFlip() tasks get executed
        win.flip()
    # mark experiment handler as finished
    thisExp.status = FINISHED
    # shut down eyetracker, if there is one
    if inputs is not None:
        if 'eyetracker' in inputs and inputs['eyetracker'] is not None:
            inputs['eyetracker'].setConnectionState(False)
    logging.flush()


def quit(thisExp, win=None, inputs=None, thisSession=None):
    """
    Fully quit, closing the window and ending the Python process.
    
    Parameters
    ==========
    win : psychopy.visual.Window
        Window to close.
    inputs : dict
        Dictionary of input devices by name.
    thisSession : psychopy.session.Session or None
        Handle of the Session object this experiment is being run from, if any.
    """
    thisExp.abort()  # or data files will save again on exit
    # make sure everything is closed down
    if win is not None:
        # Flip one final time so any remaining win.callOnFlip() 
        # and win.timeOnFlip() tasks get executed before quitting
        win.flip()
        win.close()
    if inputs is not None:
        if 'eyetracker' in inputs and inputs['eyetracker'] is not None:
            inputs['eyetracker'].setConnectionState(False)
    logging.flush()
    if thisSession is not None:
        thisSession.stop()
    # terminate Python process
    core.quit()


# if running this experiment as a script...
if __name__ == '__main__':
    # call all functions in order
    expInfo = showExpInfoDlg(expInfo=expInfo)
    thisExp = setupData(expInfo=expInfo)
    logFile = setupLogging(filename=thisExp.dataFileName)
    win = setupWindow(expInfo=expInfo)
    inputs = setupInputs(expInfo=expInfo, thisExp=thisExp, win=win)
    run(
        expInfo=expInfo, 
        thisExp=thisExp, 
        win=win, 
        inputs=inputs
    )
    saveData(thisExp=thisExp)
    quit(thisExp=thisExp, win=win, inputs=inputs)
