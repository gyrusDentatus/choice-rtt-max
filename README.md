Reaction time project in the context of the NOWA winterschool 
Authors: Alexander Goettker - NOWA Sumerschool scripts and people 

The experiment is a choice-reaction time task. For that one of three stimuli will appear on different positions on the screen, and depending on the stimulus, observers have to press a specific button. The experiment will save the reaction time, and whether the response was correct or not. 

The experiment will be programmed in Psychopy, and data is collected within the school. Analysis will be written in Python. From the raw data, we will compute averages and relate it to the age of the observers. 

Contact Information: 
Alexander Goettker
IljaistAwesome@posteo.com


MetaData 
Events: 
Stimulus  with onset, duration, trial type (which stim), rt, stim file, optional == Which response was given 
Potentially (Trial Onset with duration etc to look at sequential effects )


Behavioural Experiments 
Task Information -- > Name, Instructions, Description
Insitution Information --> Name, Adress, Department
beh.tsc = trial response response time stim_file


